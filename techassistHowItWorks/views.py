from django.shortcuts import render, redirect
from .models import Comment
from .forms import  CommentForm

# Create your views here.


def isiComment(request):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()

            # return render(request, "howitworks.html",{'form' : form})
            # return redirect('isiComment')
        

    else:
        form = CommentForm()
    comment_user = Comment.objects.all()
    

    return render(request, "howitworks.html",{'form' : form, 'comment_user' : comment_user})
    

