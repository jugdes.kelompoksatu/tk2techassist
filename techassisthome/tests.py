from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.


class UnitTest(TestCase):

    def test_landingpage_home_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_uses_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
