from django.apps import AppConfig


class PageaboutConfig(AppConfig):
    name = 'pageabout'
