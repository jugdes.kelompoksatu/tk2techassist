from django.test import TestCase, Client
from django.urls import resolve
from .forms import ContactForm
from .models import Contact
from .views import contact

# Create your tests here.

class ContactTest(TestCase):

    def test_contact_is_exist(self):
        response = Client().get('/contact/')
        self.assertEqual(response.status_code, 200)
    
    def test_contact_not_exist(self):
        response = Client().get('/adad/')
        self.assertEqual(response.status_code, 404)
    
    def test_using_contactForm(self):
        found = resolve('/contact/')
        self.assertEqual(found.func, contact)
    
    def test_uses_form_template(self):
        response = self.client.get('/contact/')
        self.assertTemplateUsed(response, 'contact.html')
    
    def test_contact_form(self):
        Contact.objects.create(yourname = "Gibran",
        email = "gibranharahap@gmail.com",
        subject = "keren")

        hitungJumlah = Contact.objects.all().count()
        self.assertEqual(hitungJumlah,1)